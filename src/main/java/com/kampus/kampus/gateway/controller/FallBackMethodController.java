package com.kampus.kampus.gateway.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FallBackMethodController {

    @GetMapping("/userApiFallBack")
    public String userApiFallBackMethod(){
        return "User Api uzun süredir yanıt vermiyor. Tekrar deneyiniz!";
    }

    @GetMapping("/managerApiFallBack")
    public String managerApiFallBackMethod(){
        return "Manage Api uzun süredir yanıt vermiyor. Tekrar deneyiniz!";
    }

    @GetMapping("/authServerFallBack")
    public String authServerFallBackMethod(){
        return "Auth Server uzun süredir yanıt vermiyor. Tekrar deneyiniz!";
    }
}
